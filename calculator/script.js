const calcForm = document.forms['calc'];
calcForm.addEventListener('submit', calculator);

function calculator(event) {
    event.preventDefault();
    document.getElementById('result').innerText = '';
    try {
        const a = prettier(calcForm.elements['first-num'].value);
        const b = prettier(calcForm.elements['second-num'].value);
        const c = prettier(calcForm.elements['third-num'].value);
        const d = prettier(calcForm.elements['fourth-num'].value);

        const operation1 = document.querySelector('input[name="operation1"]:checked').value;
        const operation2 = document.querySelector('input[name="operation2"]:checked').value;
        const operation3 = document.querySelector('input[name="operation3"]:checked').value;

        let bc = doOperation(operation2, b, c);
        document.getElementById('result').innerText = result(reversedOrder(operation1, operation3, a, bc, d));

    } catch (err) {
        console.log(`Here: ${err}`);
    }

}

function doOperation(value, b, c) {
    let res = '';
    switch (value) {
        case '+':
            res = `${sum(new Big(b), new Big(c))}`;
            break;
        case '-':
            res = `${diff(new Big(b), new Big(c))}`;
            break;
        case '*':
            res = `${multiply(new Big(b), new Big(c))}`;
            break;
        case '/':
            res = `${division(new Big(b), new Big(c))}`;
            break;
    }
    return res;
}

function reversedOrder(op1, op3, a, bc, d) {
    if ((op1 === '+' || op1 === '-') && (op3 === '*' || op3 === '/')) {
        let bcd = doOperation(op3, bc, d);
        return doOperation(op1, a, bcd);
    }
    let abc = doOperation(op1, a, bc);
    return doOperation(op3, abc, d);
}

function sum(x, y) {
    return x.plus(y).round(6).toPrecision();
}

function diff(x, y) {
    return x.minus(y).round(6).toPrecision();
}

function multiply(x, y) {
    return x.times(y).round(6).toPrecision();
}

function division(x, y) {
    return x.div(y).round(6).toPrecision();
}

function result(val) {
    let parts = val.split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    return parts.join('.');
}

function prettier(val) {
    isValid(val);
    return val.replace(',', '.').replace(/\s/g, "");
}

function isValid(val) {
    const reg = new RegExp('^(-?((\\d{1,3})( \\d{3})*)|(\\d*))((\\.\\d*)|(\,\\d*))?$')
    if ((val.indexOf('e') !== -1) || (!!!reg.test(val))) {
        throw new Error('Invalid Data');
    }
    return true;
}
